# System dump script

Dump various system informations into base64 encoded values and print them as json.

# Usage
Run a dump:
> ./sysdump.sh > dump.json

Run a dump on a remote machine using ssh:
> cat sysdump.sh | ssh user@host /bin/bash > dump.json

Extra ouput from ssh (e.g: MOTD) will be automatically cleaned by `sysdump.sh`.

To explore a dump (require `jq`), see help as follow:
> ./sysdump.sh -h

# Useful entries
Important note: Depending on the system, some entries may not be available.

| Entry                   | Description                                                      |
|-------------------------|------------------------------------------------------------------|
| dump_log                | Contains logs of errors happening during a dump                  |
| compgen                 | List all available commands in current shell                     |
| ping                    | Check if ping succeed (internet is available)                    |
| boot_folder             | Recursive listing of /boot/                                      |
| root_folder             | Content of /                                                     |
| home_\<path\>           | List the content of each home directory (at the root only)       |
| cmd_\<command\>_version | Get the version of some specific commands (e.g: gcc, python etc) |
| /proc/cmdline           | See kernel parameters                                            |
| kernel_config           | Get kernel config                                                |
| users                   | List logged users                                                |
| declare                 | Get all shell defined variables, functions etc                   |
| tar_\<path\>            | Contains specific files/directory backup (tar archives)          |

# Notes
- Some commands require root permissions. If `sysdump.sh` is not run
as root, these commands will not be executed and the corresponding
entries will not be reported in the dump file.
- Be careful, this script backup sensitive data!
